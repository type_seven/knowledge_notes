# `python中模块导入`

在此之前先说一个方法和一个变量：sys.path 和内置变量 _ _name__

我的文件目录

```
导入
|||a
|||||||||b
||||||||||||||||||-__init__
||||||||||||||||||c1.py
||||||||||||||||||c2.py
|||||||||__init__
|||||||||b1.py
|||||||||b2.py
|||__init__
|||a1.py
|||a2.py


```

sys.path:python搜索模块的路径集，返回结果是一个list集合

```python
# a1.py

import sys

print(sys.path)


# 运行结果
'''
F:\python\学习\学习demo\导入
F:\python\学习
D:\python\pycharm-3.2\PyCharm 2020.3.2\plugins\python\helpers\pycharm_display
F:\python\学习\venv\Scripts\python36.zip
D:\python\python-3.6\DLLs
D:\python\python-3.6\lib
D:\python\python-3.6
F:\python\学习\venv
F:\python\学习\venv\lib\site-packages
F:\python\学习\venv\lib\site-packages\setuptools-40.8.0-py3.6.egg
F:\python\学习\venv\lib\site-packages\pip-19.0.3-py3.6.egg
D:\python\pycharm-3.2\PyCharm 2020.3.2\plugins\python\helpers\pycharm_matplotlib_backend
'''


# c1.py

import sys

print(sys.path)


# 运行结果
'''
F:\python\学习\学习demo\导入\a\b
F:\python\学习
D:\python\pycharm-3.2\PyCharm 2020.3.2\plugins\python\helpers\pycharm_display
F:\python\学习\venv\Scripts\python36.zip
D:\python\python-3.6\DLLs
D:\python\python-3.6\lib
D:\python\python-3.6
F:\python\学习\venv
F:\python\学习\venv\lib\site-packages
F:\python\学习\venv\lib\site-packages\setuptools-40.8.0-py3.6.egg
F:\python\学习\venv\lib\site-packages\pip-19.0.3-py3.6.egg
D:\python\pycharm-3.2\PyCharm 2020.3.2\plugins\python\helpers\pycharm_matplotlib_backend
'''
```

——name—— ： 当前模块执行过程中的名称 。如果运行的就是此模块则——name——就是——main——

如果是调用此模块这表示模块的路径名称，其中最顶级的路径层级就是所运行的模块所在的路径层级

```python
# a1.py

from a.b import c2

print(__name__)


# c2.py

print('这里是c2.py：', __name__)

# 运行结果
"""
这里是c2.py: a.b.c2   #其中a1.py是运行的模块，a包是和a1.py同级的文件结果，所以这里a就是顶级名称
__main__
"""


```

 

python中的导入方式分为两种 import导入和from  import两种

import导入的最小单位是 .py文件而 from import导入的最小单位是.py文件下的变量、函数、





from import导入分为相对导入和绝对导入

绝对导入：

```python
# a1.py

from a.b import c2   #绝对导入路径参照sys.path 其中a就是F:\python\学习\学习demo\导入 下的根路径
				     #所以绝对路径就是 a.b


# 运行结果
"""
这里是c2.py: a.b.c2 

"""
```

相对导入：

相对导入就是根据——name——所表示的目录在from 和import之间加上小黑点 .  加一个表示上一级目录，两个上上级目录，依次类推，不过最高不能超过顶级目录，否则或报错[ValueError: attempted relative import beyond top-level package]()   而根据——name——的性质如果在所运行的模块中使用相对调用则会报错[ModuleNotFoundError: No module named '__main__.模块'; '__main__' is not a package]()  所以相对导入是在被调用的模块中使用的



```python
# b1.py 运行模块
from .b import c2 

# c2.py 被调用模块
print('这里是c2.py：', __name__)

# 运行结果
"""
Traceback (most recent call last):
  File "F:/python/学习/学习demo/导入/a/b1.py", line 19, in <module>
    from .b import c2
ModuleNotFoundError: No module named '__main__.b'; '__main__' is not a package

"""


# b1.py 运行模块
from b import c2 

# c2.py 被调用模块
from . import c1
print('这里是c2.py：', __name__)

# c1.py 被c2.py调用的模块
print("这里是c1.py")

# 运行结果
"""
当from和import之间的点为一个点时运行结果为：
这里是c1.py
这里是c2.py： b.c2

当from和import之间的点为两个点时运行结果为：
Traceback (most recent call last):
  File "F:/python/学习/学习demo/导入/a/b1.py", line 19, in <module>
    from b import c2
  File "F:\python\学习\学习demo\导入\a\b\c2.py", line 6, in <module>
    from .. import c1
ValueError: attempted relative import beyond top-level package
"""
```



import 导入：

python3之前的版本相当与相对导入，类似于隐式相对导入 from ... import  模块，而python3以后改为了绝对路径导入绝对路径导入是根据sys.path的路径集查找的

```python
# a1.py  运行模块
from a import b1

# b1.py  被a1调用的模块
print('这里是b1')
import b2

# b2.py 被b1调用的模块
print('这里是b2.py')

# 运行结果
# python2:
"""
D:\python\python-2.7\python.exe F:/python/学习/学习demo/导入/a1.py
这里是b1
这里是b2.py
"""

# python3: 根据sys.path绝对路径导入F:\python\学习\学习demo\导入,所以显示找不到模块b2
"""
D:\python\python-3.6\python.exe F:/python/学习/学习demo/导入/a1.py
这里是b1
Traceback (most recent call last):
  File "F:/python/学习/学习demo/导入/a1.py", line 7, in <module>
    from a import b1
  File "F:\python\学习\学习demo\导入\a\b1.py", line 20, in <module>
    import b2
ModuleNotFoundError: No module named 'b2'
"""
```



python3版本以前import是相对引用存在当自己定义的模块和内置模块定义同名的情况，内置模块无法被引用的问题。所以python3中import变成了绝对引用。那么在python2中遇到了自定义模块和内置模块同名的情况，要怎么办了？ 这里可以在import导入之前加入 [from __future__ import absolute_import]() 这段代码，代码的意思大概就是 [加入`绝对引入`这个新特性]() 。

```python
# a1.py  运行模块
from a import b1

# b1.py  被a1调用的模块
from __future__ import absolute_import # 加入绝对引入这个新特性，运行结果和python3一致
print('这里是b1')
import b2

# b2.py 被b1调用的模块
print('这里是b2.py')

# 运行结果
# python2:
"""
D:\python\python-2.7\python.exe F:/python/学习/学习demo/导入/a1.py
这里是b1
Traceback (most recent call last):
  File "F:/python/学习/学习demo/导入/a1.py", line 7, in <module>
    from a import b1
  File "F:\python\学习\学习demo\导入\a\b1.py", line 20, in <module>
    import b2
ModuleNotFoundError: No module named 'b2'
"""

```











