const express = require('express')
const sum = require('./sum')


const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));


// app.get('/get_num', function(req, res){
//     var a = parseInt(req.query.a);
//     var b = parseInt(req.query.b);
//     result = sum.add(a, b);
//     res.send(result.toString());
// });

app.get('/get_num', function(req, res){
    var a = parseInt(req.query.a);
    var b = parseInt(req.query.b);

    // 调用自己编写的 加法函数
    result = sum.add(a, b);
    res.send(result.toString());
});


app.post('/post_num', function (req, res){
    var a = req.body.a;
    var b = req.body.b;
    result = sum.add(a, b);
    res.send(result.toString());
})

// app.get('/login', function(req, res){
//     var pwd = req.query.pwd;
//     console.log(req.query);
//     pwd = login.getpwd(pwd);
//     console.log(pwd)
//     res.send(pwd.toString());
// });
app.post('/getpwd', function (req, res){
    console.log(req.body);
    // res.send(req.body);
})


app.listen(9999, () => {
    console.log('开启服务： 127.0.0.1:9999')
});