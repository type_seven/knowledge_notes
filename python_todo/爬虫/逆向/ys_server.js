// const http = require('http')
//
// const server = http.createServer(function (req, res) {
//     res.statusCode = 200;
//     res.setHeader('Content-Type', 'Content-Type', 'text/html');
//     res.end('hello world');
// });
//
// server.listen(3000, function () {
//     console.log('127.0.0.1:3000  ')
// })


const express = require('express')

const app = express();

app.get('/', (req, res) => {
    res.send('hello world')
});

app.listen(3000, () => {
    console.log('127.0.0.1:3000')
})