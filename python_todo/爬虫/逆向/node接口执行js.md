## node接口执行js

#### 使用Nodejs内置的http模块搭建服务

nodejs内置的http模块搭建node服务。

创建`server.js`，并添加如下代码：

```js
const http = require('http')

const server = http.createServer((req, res) => {
  res.statusCode = 200
  res.setHeader('Content-Type', 'text/html')
  res.end('hello world')
})

server.listen(3000, () => {
  console.log('服务已启动...');
})
```

然后在控制台中运行：

```bash
node server.js
```

#### 使用express搭建服务

##### 实例代码

```js
// 导入express 模块
const express = require('express'); 
const app = express();


// 下面两个中间件是为了处理POST 请求
// 添加中间件用于解析 JSON 数据
app.use(express.json());
// 添加中间件用于解析 URL 编码数据
app.use(express.urlencoded());


// 处理 GET 请求
app.get('/app_get', function(req, res){
    // 可以通过 req.query 获取 get 请求中的数据
    var getData = req.query;
    console.log(getData);
    
    // 注意 要执行res.send() 不然客户端会阻塞
    res.send('GET request received!');
});

// 处理 POST 请求
app.post('/app_post', (req, res) => {
    // 可以通过 req.body 获取 POST 请求中的数据
    var postData = req.body;
    console.log(postData);
    
    // 注意 要执行res.send() 不然客户端会阻塞
    res.send('POST request received!');
});

app.listen(3000, () => {
    console.log('127.0.0.1:3000');
});
```

##### 中间件的使用

在 Express 应用程序中，添加 `app.use(express.json())` 和 `app.use(express.urlencoded())` 这两行代码的目的是为了处理来自客户端的 POST 请求中的数据。

1. **`express.json()`：** 这个中间件函数使得 Express 应用程序能够解析传入请求的 JSON 数据。当客户端发送 POST 请求时，数据通常会包含在请求的正文（body）中，且格式为 JSON。如果没有添加 `express.json()` 中间件，服务器将无法正确地解析这些 JSON 数据，从而导致在处理 POST 请求时获取不到正确的数据。 通过添加这个中间件，Express 应用程序可以将请求中的 JSON 数据解析为 JavaScript 对象，并将其作为 `req.body` 对象提供给后续的请求处理函数 。
2. **`express.urlencoded()`：** 这个中间件函数用于解析传入请求的 URL 编码数据。URL 编码是一种将表单数据编码到 URL 查询字符串中的方式。当客户端使用表单提交 POST 请求时，数据通常会以 URL 编码的形式出现在请求的正文（body）中。如果没有添加 `express.urlencoded()` 中间件，服务器将无法正确解析这些 URL 编码数据。 通过添加这个中间件，Express 应用程序可以将请求中的 URL 编码数据解析为 JavaScript 对象，并将其作为 `req.body` 对象提供给后续的请求处理函数 。

##### 中间件的参数

```js
// 设置 express.json() 中间件，并添加参数
app.use(express.json({
    limit: '1mb', // 设置请求主体的大小限制为1MB
    type: 'application/json', // 设置请求的 Content-Type 为 'application/json'
}));

// 设置 express.urlencoded() 中间件，并添加参数
app.use(express.urlencoded({
    extended: false, // 使用 querystring 库解析数据（不解析复杂数据结构） 默认是 true 可省略
    limit: '10mb', // 设置请求主体的大小限制为10MB
    parameterLimit: 2000, // 限制 URL 编码参数的数量为2000
}));

// 注意：在实际开发中，通常不需要显式设置这些参数，使用默认值就能满足大部分需求。仅在有特殊需求时，你才需要指定自定义参数。
```

参数：

对于 `express.json()` 和 `express.urlencoded()` 中间件，都可以接受一些可选参数：

- **`express.json([options])`:**
  - options: 一个可选的对象，用于配置 JSON 解析的行为。
    - `limit`: 限制请求主体的大小。默认为 '100kb'。
    - `type`: 设置请求的 Content-Type。默认为 'application/json'。
- **`express.urlencoded([options])`:**
  - options: 一个可选的对象，用于配置 URL 编码数据的解析行为。
    - `extended`: 布尔值，设置为 `true` 时使用 `qs` 库解析数据；设置为 `false` 时使用 `querystring` 库解析数据，可以节省解析的性能开销。 。`qs` 库允许解析更复杂的数据结构（req.body 将会是一个对象，包含嵌套数组 ），默认为 `true`。
    - `limit`: 限制请求主体的大小。默认为 '100kb'。
    - `parameterLimit`: 用于限制 URL 编码参数的数量。默认为 1000。

这些可选参数允许你根据需要自定义数据解析的行为，比如限制请求大小、选择解析库等。在大多数情况下，你可能不需要显式设置这些参数，使用默认值就能满足大部分需求。



##### 演示 使用node 执行 某个文件的js 代码供其他人调用

```js
// sever.js  接口js 文件

// 导入express 模块
const express = require('express')

// 导入自己编写的js 模块， 在同级目录下用./模块名称
const sum = require('./own_fun')


const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));


app.get('/get_num', function(req, res){
    var a = parseInt(req.query.a);
    var b = parseInt(req.query.b);
    
    // 调用自己编写的 加法函数
    result = sum.add(a, b);
    res.send(result.toString());
});


app.post('/post_num', function (req, res){
    var a = parseInt(req.body.a);
    var b = parseInt(req.body.b);
    
    // 调用自己编写的 加法函数
    result = sum.add(a, b);
    res.send(result.toString());
})


app.listen(9999, () => {
    console.log('开启服务： 127.0.0.1:9999')
});
```

```js
// own_fun.js

function add(a, b){
    return a + b;
};

function reduce(a, b){
    return a - b;
};

// 将自己编写的js 文件导出其中的功能供其他文件使用
module.exports = {
    add,
    reduce
}

```

 在 Node.js 中，`module.exports` 是一个特殊的对象，用于在一个模块中导出（export）其功能，以便其他模块可以使用这些功能。通过 `module.exports`，你可以将一个模块中的函数、对象、类或其他数据暴露给其他模块。 